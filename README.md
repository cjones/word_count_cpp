Problem description
==========
Write a program which accepts a filename as input and counts the frequency of all the words in the file.
The program must print the top ten most frequently occurring words in the file and the count for each word in descending order.
When counting words, punctuation must be ignored and words that differ in case only should be considered the same word:
e.g. "Why", "why" and "why?" should count as 3 occurrences of "why".
The file may be assumed to contain ASCII.

You should use a Test Driven Development (TDD) approach to developing your solution.

You may use the internet to help if you wish.
If the question is ambiguous, feel free to make assumptions and state these in your solution.
Please provide your working code, unit tests, test data and any instructions necessary for compiling and running your code.

How to use this program
==========

1. use the make command: make
2. ./bin/ word_count <my_input_file.txt>

How to test this program
==========

Code borrowed from this program
==========

File structure, Makefile, and a snippit of .gitignore: https://hiltmon.com/blog/2013/07/03/a-simple-c-plus-plus-project-structure/
