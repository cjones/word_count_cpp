Things that need to be improved
==========

- Could add test cases for the printing and argparse.
- Possibly need to touch up the documentation.
- Should the main class be initialized with a file name or no?

Code review comments
==========
